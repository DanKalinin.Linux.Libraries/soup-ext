//
// Created by Dan on 10.03.2025.
//

#ifndef LIBRARY_SOUP_EXT_SOESOUPINIT_H
#define LIBRARY_SOUP_EXT_SOESOUPINIT_H

#include "soemain.h"
#include "soesouplcmessages.h"

G_BEGIN_DECLS

void soe_soup_init(void);
void soe_soup_init_i18n(gchar *directory);
void soe_soup_init_po(void);

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOESOUPINIT_H
