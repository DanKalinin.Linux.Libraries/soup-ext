//
// Created by dan on 18.11.2019.
//

#ifndef LIBRARY_SOUP_EXT_SOEHTTPERROR_H
#define LIBRARY_SOUP_EXT_SOEHTTPERROR_H

#include "soemain.h"

G_BEGIN_DECLS

gchar *soe_http_strerror(guint code);
GError *soe_http_error_new(guint code);
void soe_http_set_error(GError **self, guint code);

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOEHTTPERROR_H
