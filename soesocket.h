//
// Created by root on 20.12.2020.
//

#ifndef LIBRARY_SOUP_EXT_SOESOCKET_H
#define LIBRARY_SOUP_EXT_SOESOCKET_H

#include "soemain.h"

G_BEGIN_DECLS

GE_OBJECT_PROPERTY_NAMED(soe_socket, timeout, SoupSocket, guint)

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOESOCKET_H
