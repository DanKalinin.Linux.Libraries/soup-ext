//
// Created by dan on 09.08.2019.
//

#include "soesession.h"

enum {
    SOE_SESSION_SIGNAL_DONE = 1,
    _SOE_SESSION_SIGNAL_COUNT
};

guint soe_session_signals[_SOE_SESSION_SIGNAL_COUNT] = {0};

void soe_session_finalize(GObject *self);
void _soe_session_done(SOESession *self, SoupSession *session, SoupMessage *message);

G_DEFINE_TYPE(SOESession, soe_session, SOUP_TYPE_SESSION)

static void soe_session_class_init(SOESessionClass *class) {
    G_OBJECT_CLASS(class)->finalize = soe_session_finalize;

    class->done = _soe_session_done;

    soe_session_signals[SOE_SESSION_SIGNAL_DONE] = g_signal_new("done", SOE_TYPE_SESSION, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(SOESessionClass, done), NULL, NULL, NULL, G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER);
}

static void soe_session_init(SOESession *self) {

}

void soe_session_finalize(GObject *self) {
    G_OBJECT_CLASS(soe_session_parent_class)->finalize(self);
}

void soe_session_done_callback(SoupSession *session, SoupMessage *message, gpointer self) {
    soe_session_done(self, session, message);
}

void soe_session_done(SOESession *self, SoupSession *session, SoupMessage *message) {
    g_signal_emit(self, soe_session_signals[SOE_SESSION_SIGNAL_DONE], 0, session, message);
}

void _soe_session_done(SOESession *self, SoupSession *session, SoupMessage *message) {
    g_print("done\n");
}
