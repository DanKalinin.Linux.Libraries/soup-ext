//
// Created by dan on 20.01.2022.
//

#include "soei18n.h"

G_DEFINE_CONSTRUCTOR(soe_i18n_constructor)

static void soe_i18n_constructor(void) {
    (void)bindtextdomain(GETTEXT_PACKAGE, NULL);
    (void)bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}
