//
// Created by Dan on 20.08.2022.
//

#ifndef LIBRARY_SOUP_EXT_SOEAUTHDOMAINDIGEST_H
#define LIBRARY_SOUP_EXT_SOEAUTHDOMAINDIGEST_H

#include "soemain.h"

G_BEGIN_DECLS

#define SOE_TYPE_AUTH_DOMAIN_DIGEST soe_auth_domain_digest_get_type()

GE_DECLARE_DERIVABLE_TYPE(SOEAuthDomainDigest, soe_auth_domain_digest, SOE, AUTH_DOMAIN_DIGEST, SoupAuthDomainDigest)

struct _SOEAuthDomainDigestClass {
    SoupAuthDomainDigestClass super;

    gchar *(*auth)(SOEAuthDomainDigest *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username);
};

struct _SOEAuthDomainDigest {
    SoupAuthDomainDigest super;
};

gchar *soe_auth_domain_digest_auth_callback(SoupAuthDomain *domain, SoupMessage *message, const gchar *username, gpointer self);
gchar *soe_auth_domain_digest_auth(SOEAuthDomainDigest *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username);

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOEAUTHDOMAINDIGEST_H
