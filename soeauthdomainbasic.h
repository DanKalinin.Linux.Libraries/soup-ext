//
// Created by Dan on 21.08.2022.
//

#ifndef LIBRARY_SOUP_EXT_SOEAUTHDOMAINBASIC_H
#define LIBRARY_SOUP_EXT_SOEAUTHDOMAINBASIC_H

#include "soemain.h"

G_BEGIN_DECLS

#define SOE_TYPE_AUTH_DOMAIN_BASIC soe_auth_domain_basic_get_type()

GE_DECLARE_DERIVABLE_TYPE(SOEAuthDomainBasic, soe_auth_domain_basic, SOE, AUTH_DOMAIN_BASIC, SoupAuthDomainBasic)

struct _SOEAuthDomainBasicClass {
    SoupAuthDomainBasicClass super;

    gboolean (*auth)(SOEAuthDomainBasic *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username, gchar *password);
};

struct _SOEAuthDomainBasic {
    SoupAuthDomainBasic super;
};

gboolean soe_auth_domain_basic_auth_callback(SoupAuthDomain *domain, SoupMessage *message, const gchar *username, const gchar *password, gpointer self);
gboolean soe_auth_domain_basic_auth(SOEAuthDomainBasic *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username, gchar *password);

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOEAUTHDOMAINBASIC_H
