//
// Created by root on 19.10.2021.
//

#include "soeinit.h"
#include "soei18n.h"

void soe_init(void) {
    soe_soup_init();
}

void soe_init_i18n(gchar *directory) {
    soe_soup_init_i18n(directory);
    (void)bindtextdomain(GETTEXT_PACKAGE, directory);
    (void)bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}

void soe_init_po(void) {
    soe_soup_init_po();
    gchar *directory = bindtextdomain(GETTEXT_PACKAGE, NULL);
    gchar *language = (gchar *)g_getenv("LANG");
    g_autofree gchar *file = g_build_filename(directory, language, "LC_MESSAGES", GETTEXT_PACKAGE ".mo", NULL);
    if (!soe_lc_messages_set(file, NULL)) return;
}
