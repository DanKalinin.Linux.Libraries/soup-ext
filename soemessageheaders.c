//
// Created by dan on 27.11.2019.
//

#include "soemessageheaders.h"

void soe_message_headers_set_authorization_bearer_token(SoupMessageHeaders *self, gchar *token) {
    g_autofree gchar *value = g_strdup_printf("Bearer %s", token);
    soup_message_headers_replace(self, "Authorization", value);
}

gchar *soe_message_headers_get_authorization_bearer_token(SoupMessageHeaders *self) {
    gchar *authorization = NULL;
    if ((authorization = (gchar *)soup_message_headers_get_one(self, "Authorization")) == NULL) return NULL;

//    gchar *ret = NULL;
//    if (sscanf(authorization, "Bearer %ms", &ret) != 1) return NULL;
//    return ret;

    gchar ret[strlen(authorization) - 6];
    if (sscanf(authorization, "Bearer %s", ret) != 1) return NULL;
    return g_strdup(ret);
}

void soe_message_headers_set_authorization_basic_credentials(SoupMessageHeaders *self, gchar *username, gchar *password) {
    g_autofree gchar *credentials = g_strdup_printf("%s:%s", username, password);
    gsize n = strlen(credentials);
    g_autofree gchar *credentials64 = g_base64_encode((guchar *)credentials, n);
    g_autofree gchar *value = g_strdup_printf("Basic %s", credentials64);
    soup_message_headers_replace(self, "Authorization", value);
}

void soe_message_headers_set_accept_encoding(SoupMessageHeaders *self, gchar *accept_encoding) {
    soup_message_headers_replace(self, "Accept-Encoding", accept_encoding);
}
