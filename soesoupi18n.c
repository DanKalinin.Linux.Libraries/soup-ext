//
// Created by Dan on 10.03.2025.
//

#include "soesoupi18n.h"

G_DEFINE_CONSTRUCTOR(soe_soup_i18n_constructor)

static void soe_soup_i18n_constructor(void) {
    (void)bindtextdomain(GETTEXT_PACKAGE, NULL);
    (void)bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}
