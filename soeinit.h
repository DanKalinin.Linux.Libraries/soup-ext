//
// Created by root on 19.10.2021.
//

#ifndef LIBRARY_SOUP_EXT_SOEINIT_H
#define LIBRARY_SOUP_EXT_SOEINIT_H

#include "soemain.h"
#include "soelcmessages.h"
#include "soesoupinit.h"

G_BEGIN_DECLS

void soe_init(void);
void soe_init_i18n(gchar *directory);
void soe_init_po(void);

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOEINIT_H
