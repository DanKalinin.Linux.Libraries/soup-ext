//
// Created by dan on 10.08.2019.
//

#include "soemessage.h"

void soe_message_finalize(GObject *self);

G_DEFINE_TYPE(SOEMessage, soe_message, SOUP_TYPE_MESSAGE)

static void soe_message_class_init(SOEMessageClass *class) {
    G_OBJECT_CLASS(class)->finalize = soe_message_finalize;
}

static void soe_message_init(SOEMessage *self) {

}

void soe_message_finalize(GObject *self) {
    G_OBJECT_CLASS(soe_message_parent_class)->finalize(self);
}
