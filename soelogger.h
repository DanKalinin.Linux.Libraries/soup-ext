//
// Created by dan on 10.08.2019.
//

#ifndef LIBRARY_SOUP_EXT_SOELOGGER_H
#define LIBRARY_SOUP_EXT_SOELOGGER_H

#include "soemain.h"

G_BEGIN_DECLS

#define SOE_TYPE_LOGGER soe_logger_get_type()

GE_DECLARE_DERIVABLE_TYPE(SOELogger, soe_logger, SOE, LOGGER, SoupLogger)

struct _SOELoggerClass {
    SoupLoggerClass super;
};

struct _SOELogger {
    SoupLogger super;
};

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOELOGGER_H
