//
// Created by Dan on 09.03.2025.
//

#include "soesouplcmessages.h"

gboolean soe_soup_lc_messages_set(gchar *file, GError **error) {
    gchar *language = (gchar *)g_getenv("LANG");
    if (g_str_equal(language, "ru")) return ge_file_set_contents_mkdir(file, soe_soup_lc_messages_ru_data, soe_soup_lc_messages_ru_n, error);
    return TRUE;
}
