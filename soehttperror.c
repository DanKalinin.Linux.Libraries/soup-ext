//
// Created by dan on 18.11.2019.
//

#include "soehttperror.h"
#include "soei18n.h"

gchar *soe_http_strerror(guint code) {
    if (code == SOUP_STATUS_CANCELLED) {
        return _("Cancelled");
    } else if (code == SOUP_STATUS_CANT_RESOLVE) {
        return _("Cannot resolve hostname");
    } else if (code == SOUP_STATUS_CANT_RESOLVE_PROXY) {
        return _("Cannot resolve proxy hostname");
    } else if (code == SOUP_STATUS_CANT_CONNECT) {
        return _("Cannot connect to destination");
    } else if (code == SOUP_STATUS_CANT_CONNECT_PROXY) {
        return _("Cannot connect to proxy");
    } else if (code == SOUP_STATUS_SSL_FAILED) {
        return _("SSL handshake failed");
    } else if (code == SOUP_STATUS_IO_ERROR) {
        return _("Connection terminated unexpectedly");
    } else if (code == SOUP_STATUS_MALFORMED) {
        return _("Message Corrupt");
    } else if (code == SOUP_STATUS_TOO_MANY_REDIRECTS) {
        return _("Too many redirects");
    } else if (code == SOUP_STATUS_CONTINUE) {
        return _("Continue");
    } else if (code == SOUP_STATUS_SWITCHING_PROTOCOLS) {
        return _("Switching Protocols");
    } else if (code == SOUP_STATUS_PROCESSING) {
        return _("Processing");
    } else if (code == SOUP_STATUS_OK) {
        return _("OK");
    } else if (code == SOUP_STATUS_CREATED) {
        return _("Created");
    } else if (code == SOUP_STATUS_ACCEPTED) {
        return _("Accepted");
    } else if (code == SOUP_STATUS_NON_AUTHORITATIVE) {
        return _("Non-Authoritative Information");
    } else if (code == SOUP_STATUS_NO_CONTENT) {
        return _("No Content");
    } else if (code == SOUP_STATUS_RESET_CONTENT) {
        return _("Reset Content");
    } else if (code == SOUP_STATUS_PARTIAL_CONTENT) {
        return _("Partial Content");
    } else if (code == SOUP_STATUS_MULTI_STATUS) {
        return _("Multi-Status");
    } else if (code == SOUP_STATUS_MULTIPLE_CHOICES) {
        return _("Multiple Choices");
    } else if (code == SOUP_STATUS_MOVED_PERMANENTLY) {
        return _("Moved Permanently");
    } else if (code == SOUP_STATUS_FOUND) {
        return _("Found");
    } else if (code == SOUP_STATUS_SEE_OTHER) {
        return _("See Other");
    } else if (code == SOUP_STATUS_NOT_MODIFIED) {
        return _("Not Modified");
    } else if (code == SOUP_STATUS_USE_PROXY) {
        return _("Use Proxy");
    } else if (code == SOUP_STATUS_TEMPORARY_REDIRECT) {
        return _("Temporary Redirect");
    } else if (code == SOUP_STATUS_BAD_REQUEST) {
        return _("Bad Request");
    } else if (code == SOUP_STATUS_UNAUTHORIZED) {
        return _("Unauthorized");
    } else if (code == SOUP_STATUS_PAYMENT_REQUIRED) {
        return _("Payment Required");
    } else if (code == SOUP_STATUS_FORBIDDEN) {
        return _("Forbidden");
    } else if (code == SOUP_STATUS_NOT_FOUND) {
        return _("Not Found");
    } else if (code == SOUP_STATUS_METHOD_NOT_ALLOWED) {
        return _("Method Not Allowed");
    } else if (code == SOUP_STATUS_NOT_ACCEPTABLE) {
        return _("Not Acceptable");
    } else if (code == SOUP_STATUS_PROXY_UNAUTHORIZED) {
        return _("Proxy Authentication Required");
    } else if (code == SOUP_STATUS_REQUEST_TIMEOUT) {
        return _("Request Timeout");
    } else if (code == SOUP_STATUS_CONFLICT) {
        return _("Conflict");
    } else if (code == SOUP_STATUS_GONE) {
        return _("Gone");
    } else if (code == SOUP_STATUS_LENGTH_REQUIRED) {
        return _("Length Required");
    } else if (code == SOUP_STATUS_PRECONDITION_FAILED) {
        return _("Precondition Failed");
    } else if (code == SOUP_STATUS_REQUEST_ENTITY_TOO_LARGE) {
        return _("Request Entity Too Large");
    } else if (code == SOUP_STATUS_REQUEST_URI_TOO_LONG) {
        return _("Request-URI Too Long");
    } else if (code == SOUP_STATUS_UNSUPPORTED_MEDIA_TYPE) {
        return _("Unsupported Media Type");
    } else if (code == SOUP_STATUS_INVALID_RANGE) {
        return _("Requested Range Not Satisfiable");
    } else if (code == SOUP_STATUS_EXPECTATION_FAILED) {
        return _("Expectation Failed");
    } else if (code == SOUP_STATUS_UNPROCESSABLE_ENTITY) {
        return _("Unprocessable Entity");
    } else if (code == SOUP_STATUS_LOCKED) {
        return _("Locked");
    } else if (code == SOUP_STATUS_FAILED_DEPENDENCY) {
        return _("Failed Dependency");
    } else if (code == SOUP_STATUS_INTERNAL_SERVER_ERROR) {
        return _("Internal Server Error");
    } else if (code == SOUP_STATUS_NOT_IMPLEMENTED) {
        return _("Not Implemented");
    } else if (code == SOUP_STATUS_BAD_GATEWAY) {
        return _("Bad Gateway");
    } else if (code == SOUP_STATUS_SERVICE_UNAVAILABLE) {
        return _("Service Unavailable");
    } else if (code == SOUP_STATUS_GATEWAY_TIMEOUT) {
        return _("Gateway Timeout");
    } else if (code == SOUP_STATUS_HTTP_VERSION_NOT_SUPPORTED) {
        return _("HTTP Version Not Supported");
    } else if (code == SOUP_STATUS_INSUFFICIENT_STORAGE) {
        return _("Insufficient Storage");
    } else if (code == SOUP_STATUS_NOT_EXTENDED) {
        return _("Not Extended");
    } else {
        return _("Unknown error");
    }
}

GError *soe_http_error_new(guint code) {
    gchar *message = soe_http_strerror(code);
    GError *self = g_error_new_literal(SOUP_HTTP_ERROR, code, message);
    return self;
}

void soe_http_set_error(GError **self, guint code) {
    gchar *message = soe_http_strerror(code);
    g_set_error_literal(self, SOUP_HTTP_ERROR, code, message);
}
