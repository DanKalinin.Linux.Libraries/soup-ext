//
// Created by dan on 09.08.2019.
//

#ifndef LIBRARY_SOUP_EXT_SOESESSION_H
#define LIBRARY_SOUP_EXT_SOESESSION_H

#include "soemain.h"










G_BEGIN_DECLS

GE_OBJECT_PROPERTY_NAMED(soe_session, timeout, SoupSession, guint)
GE_OBJECT_PROPERTY_NAMED(soe_session, idle_timeout, SoupSession, guint)

G_END_DECLS










G_BEGIN_DECLS

#define SOE_TYPE_SESSION soe_session_get_type()

GE_DECLARE_DERIVABLE_TYPE(SOESession, soe_session, SOE, SESSION, SoupSession)

struct _SOESessionClass {
    SoupSessionClass super;

    void (*done)(SOESession *self, SoupSession *session, SoupMessage *message);
};

struct _SOESession {
    SoupSession super;
};

void soe_session_done_callback(SoupSession *session, SoupMessage *message, gpointer self);
void soe_session_done(SOESession *self, SoupSession *session, SoupMessage *message);

G_END_DECLS










#endif //LIBRARY_SOUP_EXT_SOESESSION_H
