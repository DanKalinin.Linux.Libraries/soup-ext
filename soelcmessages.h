//
// Created by Dan on 05.03.2025.
//

#ifndef LIBRARY_SOUP_EXT_SOELCMESSAGES_H
#define LIBRARY_SOUP_EXT_SOELCMESSAGES_H

#include "soemain.h"

G_BEGIN_DECLS

extern gchar soe_lc_messages_ru_data[];
extern gint soe_lc_messages_ru_n;

gboolean soe_lc_messages_set(gchar *file, GError **error);

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOELCMESSAGES_H
