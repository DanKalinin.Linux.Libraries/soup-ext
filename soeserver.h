//
// Created by dan on 11.08.2019.
//

#ifndef LIBRARY_SOUP_EXT_SOESERVER_H
#define LIBRARY_SOUP_EXT_SOESERVER_H

#include "soemain.h"

G_BEGIN_DECLS

#define SOE_TYPE_SERVER soe_server_get_type()

GE_DECLARE_DERIVABLE_TYPE(SOEServer, soe_server, SOE, SERVER, SoupServer)

struct _SOEServerClass {
    SoupServerClass super;
};

struct _SOEServer {
    SoupServer super;
};

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOESERVER_H
