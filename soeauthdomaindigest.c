//
// Created by Dan on 20.08.2022.
//

#include "soeauthdomaindigest.h"

enum {
    SOE_AUTH_DOMAIN_DIGEST_SIGNAL_AUTH = 1,
    _SOE_AUTH_DOMAIN_DIGEST_SIGNAL_COUNT
};

guint soe_auth_domain_digest_signals[_SOE_AUTH_DOMAIN_DIGEST_SIGNAL_COUNT] = {0};

gchar *_soe_auth_domain_digest_auth(SOEAuthDomainDigest *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username);

G_DEFINE_TYPE(SOEAuthDomainDigest, soe_auth_domain_digest, SOUP_TYPE_AUTH_DOMAIN_DIGEST)

static void soe_auth_domain_digest_class_init(SOEAuthDomainDigestClass *class) {
    class->auth = _soe_auth_domain_digest_auth;

    soe_auth_domain_digest_signals[SOE_AUTH_DOMAIN_DIGEST_SIGNAL_AUTH] = g_signal_new("auth", SOE_TYPE_AUTH_DOMAIN_DIGEST, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(SOEAuthDomainDigestClass, auth), NULL, NULL, NULL, G_TYPE_POINTER, 3, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_POINTER);
}

static void soe_auth_domain_digest_init(SOEAuthDomainDigest *self) {

}

gchar *soe_auth_domain_digest_auth_callback(SoupAuthDomain *domain, SoupMessage *message, const gchar *username, gpointer self) {
    gchar *ret = soe_auth_domain_digest_auth(self, domain, message, (gchar *)username);
    return ret;
}

gchar *soe_auth_domain_digest_auth(SOEAuthDomainDigest *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username) {
    gchar *ret = NULL;
    g_signal_emit(self, soe_auth_domain_digest_signals[SOE_AUTH_DOMAIN_DIGEST_SIGNAL_AUTH], 0, domain, message, username, &ret);
    return ret;
}

gchar *_soe_auth_domain_digest_auth(SOEAuthDomainDigest *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username) {
    g_print("auth\n");
    return NULL;
}
