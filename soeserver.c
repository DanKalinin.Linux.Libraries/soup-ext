//
// Created by dan on 11.08.2019.
//

#include "soeserver.h"

void soe_server_finalize(GObject *self);

G_DEFINE_TYPE(SOEServer, soe_server, SOUP_TYPE_SERVER)

static void soe_server_class_init(SOEServerClass *class) {
    G_OBJECT_CLASS(class)->finalize = soe_server_finalize;
}

static void soe_server_init(SOEServer *self) {

}

void soe_server_finalize(GObject *self) {
    G_OBJECT_CLASS(soe_server_parent_class)->finalize(self);
}
