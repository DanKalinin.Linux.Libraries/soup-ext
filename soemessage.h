//
// Created by dan on 10.08.2019.
//

#ifndef LIBRARY_SOUP_EXT_SOEMESSAGE_H
#define LIBRARY_SOUP_EXT_SOEMESSAGE_H

#include "soemain.h"

G_BEGIN_DECLS

#define SOE_TYPE_MESSAGE soe_message_get_type()

GE_DECLARE_DERIVABLE_TYPE(SOEMessage, soe_message, SOE, MESSAGE, SoupMessage)

struct _SOEMessageClass {
    SoupMessageClass super;
};

struct _SOEMessage {
    SoupMessage super;
};

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOEMESSAGE_H
