//
// Created by Dan on 21.08.2022.
//

#include "soeauthdomainbasic.h"

enum {
    SOE_AUTH_DOMAIN_BASIC_SIGNAL_AUTH = 1,
    _SOE_AUTH_DOMAIN_BASIC_SIGNAL_COUNT
};

guint soe_auth_domain_basic_signals[_SOE_AUTH_DOMAIN_BASIC_SIGNAL_COUNT] = {0};

gboolean _soe_auth_domain_basic_auth(SOEAuthDomainBasic *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username, gchar *password);

G_DEFINE_TYPE(SOEAuthDomainBasic, soe_auth_domain_basic, SOUP_TYPE_AUTH_DOMAIN_BASIC)

static void soe_auth_domain_basic_class_init(SOEAuthDomainBasicClass *class) {
    class->auth = _soe_auth_domain_basic_auth;

    soe_auth_domain_basic_signals[SOE_AUTH_DOMAIN_BASIC_SIGNAL_AUTH] = g_signal_new("auth", SOE_TYPE_AUTH_DOMAIN_BASIC, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(SOEAuthDomainBasicClass, auth), NULL, NULL, NULL, G_TYPE_BOOLEAN, 4, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_POINTER);
}

static void soe_auth_domain_basic_init(SOEAuthDomainBasic *self) {

}

gboolean soe_auth_domain_basic_auth_callback(SoupAuthDomain *domain, SoupMessage *message, const gchar *username, const gchar *password, gpointer self) {
    gboolean ret = soe_auth_domain_basic_auth(self, domain, message, (gchar *)username, (gchar *)password);
    return ret;
}

gboolean soe_auth_domain_basic_auth(SOEAuthDomainBasic *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username, gchar *password) {
    gboolean ret = FALSE;
    g_signal_emit(self, soe_auth_domain_basic_signals[SOE_AUTH_DOMAIN_BASIC_SIGNAL_AUTH], 0, domain, message, username, password, &ret);
    return ret;
}

gboolean _soe_auth_domain_basic_auth(SOEAuthDomainBasic *self, SoupAuthDomain *domain, SoupMessage *message, gchar *username, gchar *password) {
    g_print("auth\n");
    return FALSE;
}
