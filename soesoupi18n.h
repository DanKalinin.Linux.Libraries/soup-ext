//
// Created by Dan on 10.03.2025.
//

#ifndef LIBRARY_SOUP_EXT_SOESOUPI18N_H
#define LIBRARY_SOUP_EXT_SOESOUPI18N_H

#include "soemain.h"

G_BEGIN_DECLS

#define GETTEXT_PACKAGE "libsoup"
#include <glib/gi18n-lib.h>

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOESOUPI18N_H
