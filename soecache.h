//
// Created by dan on 10.08.2019.
//

#ifndef LIBRARY_SOUP_EXT_SOECACHE_H
#define LIBRARY_SOUP_EXT_SOECACHE_H

#include "soemain.h"

G_BEGIN_DECLS

#define SOE_TYPE_CACHE soe_cache_get_type()

GE_DECLARE_DERIVABLE_TYPE(SOECache, soe_cache, SOE, CACHE, SoupCache)

struct _SOECacheClass {
    SoupCacheClass super;
};

struct _SOECache {
    SoupCache super;
};

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOECACHE_H
