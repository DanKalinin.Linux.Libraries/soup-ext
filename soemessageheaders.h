//
// Created by dan on 27.11.2019.
//

#ifndef LIBRARY_SOUP_EXT_SOEMESSAGEHEADERS_H
#define LIBRARY_SOUP_EXT_SOEMESSAGEHEADERS_H

#include "soemain.h"

G_BEGIN_DECLS

void soe_message_headers_set_authorization_bearer_token(SoupMessageHeaders *self, gchar *token);
gchar *soe_message_headers_get_authorization_bearer_token(SoupMessageHeaders *self);

void soe_message_headers_set_authorization_basic_credentials(SoupMessageHeaders *self, gchar *username, gchar *password);

void soe_message_headers_set_accept_encoding(SoupMessageHeaders *self, gchar *accept_encoding);

G_END_DECLS

#endif //LIBRARY_SOUP_EXT_SOEMESSAGEHEADERS_H
